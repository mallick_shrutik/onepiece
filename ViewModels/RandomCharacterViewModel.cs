﻿using OnePiece.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnePiece.ViewModels
{
    public class RandomCharacterViewModel
    {
        public Character Character { get; set; }
        public Manga Manga { get; set; }
        public List<Manga> Mangas { get; set; }
        public List<Character> Characters { get; internal set; }
    }
}