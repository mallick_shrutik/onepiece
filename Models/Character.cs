﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnePiece.Models
{
    //Character class is a POCO(Plain Old CLR Object). 
    //POCOs do not have a behaviour or logic but has some properties that are used to represent states
    public class Character
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}