﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnePiece.Models;
using OnePiece.ViewModels;

namespace OnePiece.Controllers
{
    public class CharactersController : Controller
    {
        // GET: Characters
        public ActionResult Random()
        {
            /*var character = new Character {
                Name = "Monkey D. Luffy"
            };*/
            var characters = new List<Character>
            {
                new Character
                {
                    Id = 1,
                    Name = "Straw Hat Pirates"
                },
                new Character
                {
                    Id = 2,
                    Name = "Animal Kingdom Pirates"
                },
                new Character
                {
                    Id = 3,
                    Name = "Arlong Pirates"
                },
                new Character
                {
                    Id = 4,
                    Name = "Bellamy Pirates"
                },
                new Character
                {
                    Id = 5,
                    Name = "Blackbeard Pirates"
                },
                
            };
            return View("Random", characters);
        }

        [Route("characters/group/{groupName}")]
        public ActionResult ByGroupName(string groupName)
        {
            return Content(groupName);
        }
    }
}

/*
3.1 Animal Kingdom Pirates
3.2	Arlong Pirates
3.3	Bellamy Pirates
3.4	Blackbeard Pirates
3.5	Bliking Pirates
3.6	Big Mom Pirates
3.7	Black Cat Pirates
3.8	Buggy's Band of Pirates
3.9	Caribou
3.10	Dracule Mihawk
3.11	Don Quixote Pirates
3.12	Edward Weevil
3.13	Four Emperors
3.14	Foxy pirates
3.15	Krieg Pirates
3.16	Kuja
3.17	Monkey Mountain Allied Force
3.18	New Fishman Pirates
3.19	Red-haired pirates
3.20	Roger Pirates
3.21	Rocks Pirates
3.22	Straw Hat Grand Fleet
3.23	Sun Pirates
3.24	Thriller Bark Pirates
3.25	Whitebeard Pirates
3.26	Worst generation
*/